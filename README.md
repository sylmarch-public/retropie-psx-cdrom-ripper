# retropie-psx-cdrom-ripper

[[_TOC_]]

## :warning: IMPORTANT - DISCLAIMER :warning:

**Check if using this script is legal in your country.**

**Don't rip a CD-ROM if you don't have the original CD-ROM.**

**Don't share generated ROMs. Use them only for your personal usage (e.g. to run games with RetroPie when your PSX/PlayStation 1 console is broken).**

## Description

Script to **rip a PSX/PlayStation 1 game**, i.e. to create a ROM from PSX/PlayStation 1 CD-ROM.

Generated files are **compliant with [RetroPie stack](https://retropie.org.uk/)**.

Following files are generated for the game `<name>`:

- for **single-disc** game, 2 files:
    - Data: `<name>.bin`
    - Metadata: `<name>.cue`
- for **multi-disc** game:
    - for each CD-ROM, where `X` is the index of the CD-ROM:
        - Data: `<name> (Disc X).bin`
        - Metadata: `<name> (Disc X).CDX` (they are `cue` files)
    - 1 global file `<name>.m3u` that lists those `.CDX` files.

This script has been successfully tested on Debian 11, using:

- RetroPie v4.8.2
- EmulationStation v2.11.0RP
- BIOS `scph5501.bin` (sha256: `11052b6499e466bbf0a709b1f9cb6834a9418e66680387912451e971cf8a1fef`)
- French PSX original CD-ROM (PAL)

Launch the script with `--help` option to discover own to use it.

## Releases

To know what's new in a version, see the release notes on the [releases page](https://gitlab.com/sylmarch-public/retropie-psx-cdrom-ripper/-/releases).

You can also directly download the script from the last release using [this permalink](https://gitlab.com/sylmarch-public/retropie-psx-cdrom-ripper/-/releases/permalink/latest/downloads/retropie-psx-cdrom-ripper.sh).

## Dependencies

This script has 2 dependencies. You must install these commands:

- `cdrdao`: reads and writes CDs in disc-at-once mode.
- `toc2cue`: converts a TOC files of cdrdao(1) into a .cue file.

On Debian: `apt install cdrdao cue2toc`

## Single-Disc games VS Multi-Disc games

By default, the script will rip the game as a **single-disc game**.

When multiple discs should be ripped (**multi-disc game**), you must use the option `--disc <number-of-discs` to set the number of discs to rip.

RetroPie describes [how to handle multi-disc game in its documentation](https://retropie.org.uk/docs/Playstation-1/#m3u-playlists-for-cue-bins-or-chds).

## Output folder

The script assumes that RetroPie PSX folder is located at `${HOME}/RetroPie/roms/psx/`.

Generated files will be automatically deployed in it.

You can change destination folder using `--output-dir <path>` option.

## Examples

### Single-disc game (i.e. only 1 CD-ROM)

Hereby ripping Tony Hawk's Pro Skater 2 (only 1 CD-ROM).

After ripping, you have just to restart EmulationStation and the game should appear in PlayStation section.

```bash
$ ./retropie-psx-cdrom-ripper.sh --name "Tony Hawk's Pro Skater 2"
19:33:17 [INFO] Video game:          Tony Hawk's Pro Skater 2
19:33:17 [INFO] CD-ROM device:       /dev/sr0
19:33:17 [INFO] Type :               Single-disc game (only 1 CD-ROM)
19:33:17 [INFO] Output directory:    /home/yourlogin/RetroPie/roms/psx
19:33:17 [INFO] Temporary directory: /tmp/retropie-psx-cdrom-ripper
19:33:17 [INFO] Creating temporary directory /tmp/retropie-psx-cdrom-ripper...
19:33:17 [INFO] ===== Ripping CD-ROM n°1/1 =====
19:33:17 [INFO] Ripping CD-ROM...
19:33:17 [INFO]  - Temporary .bin path:   /tmp/retropie-psx-cdrom-ripper/Tony Hawk's Pro Skater 2.bin
19:33:17 [INFO]  - Temporary .cue path:   /tmp/retropie-psx-cdrom-ripper/Tony Hawk's Pro Skater 2.cue
19:33:17 [INFO]  - Temporary .toc path:   /tmp/retropie-psx-cdrom-ripper/Tony Hawk's Pro Skater 2.toc
19:33:17 [INFO]  - Output .bin path:      /home/yourlogin/RetroPie/roms/psx/Tony Hawk's Pro Skater 2.bin
19:33:17 [INFO]  - Output .cue path:      /home/yourlogin/RetroPie/roms/psx/Tony Hawk's Pro Skater 2.cue
Cdrdao version 1.2.4 - (C) Andreas Mueller <andreas@daneb.de>
/dev/sr0: TSSTcorp CDDVDW SH-224DB      Rev: SB01
Using driver: Generic SCSI-3/MMC (raw writing) - Version 2.0 (options 0x0000)

Reading toc and track data...

Track   Mode    Flags  Start                Length
------------------------------------------------------------
 1      DATA    4      00:00:00(     0)     70:39:44(317969)
Leadout DATA    4      70:39:44(317969)

PQ sub-channel reading (data track) is supported, data format is BCD.
Raw P-W sub-channel reading (data track) is supported.
Cooked R-W sub-channel reading (data track) is supported.
Copying data track 1 (MODE2_RAW): start 00:00:00, length 70:39:44 to "/tmp/Tony Hawk's Pro Skater 2.bin"...
Reading of toc and track data finished successfully.
19:35:14 [INFO] Generating .cue file...
toc2cue version 1.2.4 - (C) Andreas Mueller <andreas@daneb.de>

Converted toc-file '/tmp/Tony Hawk's Pro Skater 2.toc' to cue file '/tmp/Tony Hawk's Pro Skater 2.cue'.

Please note that the resulting cue file is only valid if the
toc-file was created with cdrdao using the commands 'read-toc'
or 'read-cd'. For manually created or edited toc-files the
cue file may not be correct. This program just checks for
the most obvious toc-file features that cannot be converted to
a cue file.
Furthermore, if the toc-file contains audio tracks the byte
order of the image file will be wrong which results in static
noise when the resulting cue file is used for recording
(even with cdrdao itself).
19:37:11 [INFO] Patching path in .cue file...
19:37:11 [INFO] Deploying .bin and .cue file to output directory...
19:37:11 [INFO] Removing temporary .toc file...
19:37:11 [INFO] Operation completed for current CD-ROM:
19:37:11 [INFO]  - Binary file: /home/yourlogin/RetroPie/roms/psx/Tony Hawk's Pro Skater 2.bin
19:37:11 [INFO]  - CUE file:    /home/yourlogin/RetroPie/roms/psx/Tony Hawk's Pro Skater 2.cue
19:37:11 [INFO] All CD-ROMs have been ripped (1 CD-ROMs).
19:37:11 [INFO] Removing temporary directory /tmp/retropie-psx-cdrom-ripper...
19:37:11 [INFO] ===== Operation completed =====
19:37:11 [INFO] Output files:
19:37:11 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Tony Hawk's Pro Skater 2.bin
19:37:11 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Tony Hawk's Pro Skater 2.cue

```

### Multi-disc game (i.e. 2 or more CD-ROMs)

Hereby ripping Metal Gear Solid (2 CD-ROMs).

After ripping, you have just to restart EmulationStation and the game should appear in PlayStation section.

```bash
$ ./retropie-psx-cdrom-ripper.sh --name "Metal Gear Solid" --disc 2
19:14:19 [INFO] Video game:          Metal Gear Solid
19:14:19 [INFO] CD-ROM device:       /dev/sr0
19:14:19 [INFO] Type :               Multi-discs games (2 CD-ROMS)
19:14:19 [INFO] Output directory:    /home/yourlogin/RetroPie/roms/psx
19:14:19 [INFO] Temporary directory: /tmp/retropie-psx-cdrom-ripper
19:14:19 [INFO] Creating temporary directory /tmp/retropie-psx-cdrom-ripper...
19:14:19 [INFO] ===== Ripping CD-ROM n°1/2 =====
19:14:19 [INFO] Ripping CD-ROM...
19:14:19 [INFO]  - Temporary .bin path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 1).bin
19:14:19 [INFO]  - Temporary .cue path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 1).CD1
19:14:19 [INFO]  - Temporary .toc path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 1).toc
19:14:19 [INFO]  - Output .bin path:      /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 1).bin
19:14:19 [INFO]  - Output .cue path:      /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 1).CD1
Cdrdao version 1.2.4 - (C) Andreas Mueller <andreas@daneb.de>
/dev/sr0: TSSTcorp CDDVDW SH-224DB Rev: SB01
Using driver: Generic SCSI-3/MMC (raw writing) - Version 2.0 (options 0x0000)

Reading toc and track data...

Track   Mode    Flags  Start                Length
------------------------------------------------------------
 1      DATA    4      00:00:00(     0)     63:42:59(286709)
Leadout DATA    4      63:42:59(286709)

PQ sub-channel reading (data track) is supported, data format is BCD.
Raw P-W sub-channel reading (data track) is supported.
Cooked R-W sub-channel reading (data track) is supported.
Copying data track 1 (MODE2_RAW): start 00:00:00, length 63:42:59 to "/tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 1).bin"...
Reading of toc and track data finished successfully.
19:21:06 [INFO] Generating .cue file...
toc2cue version 1.2.4 - (C) Andreas Mueller <andreas@daneb.de>

Converted toc-file '/tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 1).toc' to cue file '/tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 1).CD1'.

Please note that the resulting cue file is only valid if the
toc-file was created with cdrdao using the commands 'read-toc'
or 'read-cd'. For manually created or edited toc-files the
cue file may not be correct. This program just checks for
the most obvious toc-file features that cannot be converted to
a cue file.
Furthermore, if the toc-file contains audio tracks the byte
order of the image file will be wrong which results in static
noise when the resulting cue file is used for recording
(even with cdrdao itself).
19:21:06 [INFO] Patching path in .cue file...
19:21:06 [INFO] Deploying .bin and .cue file to output directory...
19:21:07 [INFO] Removing temporary .toc file...
19:21:07 [INFO] Operation completed for current CD-ROM:
19:21:07 [INFO]  - Binary file: /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 1).bin
19:21:07 [INFO]  - CUE file:    /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 1).CD1

Please, insert next CD. Then, press ENTER to start ripping it.
19:21:32 [INFO] ===== Ripping CD-ROM n°2/2 =====
19:21:32 [INFO] Ripping CD-ROM...
19:21:32 [INFO]  - Temporary .bin path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 2).bin
19:21:33 [INFO]  - Temporary .cue path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 2).CD2
19:21:33 [INFO]  - Temporary .toc path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 2).toc
19:21:33 [INFO]  - Output .bin path:      /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 2).bin
19:21:33 [INFO]  - Output .cue path:      /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 2).CD2
Cdrdao version 1.2.4 - (C) Andreas Mueller <andreas@daneb.de>
/dev/sr0: TSSTcorp CDDVDW SH-224DB Rev: SB01
Using driver: Generic SCSI-3/MMC (raw writing) - Version 2.0 (options 0x0000)

Reading toc and track data...

Track   Mode    Flags  Start                Length
------------------------------------------------------------
 1      DATA    4      00:00:00(     0)     66:15:10(298135)
Leadout DATA    4      66:15:10(298135)

PQ sub-channel reading (data track) is supported, data format is BCD.
Raw P-W sub-channel reading (data track) is supported.
Cooked R-W sub-channel reading (data track) is supported.
Copying data track 1 (MODE2_RAW): start 00:00:00, length 66:15:10 to "/tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 2).bin"...
Reading of toc and track data finished successfully.
19:29:21 [INFO] Generating .cue file...
toc2cue version 1.2.4 - (C) Andreas Mueller <andreas@daneb.de>

Converted toc-file '/tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 2).toc' to cue file '/tmp/retropie-psx-cdrom-ripper/Metal Gear Solid (Disc 2).CD2'.

Please note that the resulting cue file is only valid if the
toc-file was created with cdrdao using the commands 'read-toc'
or 'read-cd'. For manually created or edited toc-files the
cue file may not be correct. This program just checks for
the most obvious toc-file features that cannot be converted to
a cue file.
Furthermore, if the toc-file contains audio tracks the byte
order of the image file will be wrong which results in static
noise when the resulting cue file is used for recording
(even with cdrdao itself).
19:29:21 [INFO] Patching path in .cue file...
19:29:21 [INFO] Deploying .bin and .cue file to output directory...
19:29:22 [INFO] Removing temporary .toc file...
19:29:22 [INFO] Operation completed for current CD-ROM:
19:29:22 [INFO]  - Binary file: /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 2).bin
19:29:22 [INFO]  - CUE file:    /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 2).CD2
19:29:22 [INFO] All CD-ROMs have been ripped (2 CD-ROMs).
19:29:22 [INFO] ===== Generating .m3u file for multi-disc game =====
19:29:22 [INFO] Generating .m3u file...
19:29:22 [INFO]  - Temporary .m3u path:   /tmp/retropie-psx-cdrom-ripper/Metal Gear Solid.m3u
19:29:22 [INFO]  - Output .m3u path:      /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid.m3u
19:29:22 [INFO] Deploying .m3u file to output directory...
19:29:22 [INFO] Removing temporary directory /tmp/retropie-psx-cdrom-ripper...
19:29:22 [INFO] ===== Operation completed =====
19:29:22 [INFO] Output files:
19:29:22 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 1).bin
19:29:22 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 1).CD1
19:29:22 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 2).bin
19:29:22 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid (Disc 2).CD2
19:29:22 [INFO]  - /home/yourlogin/RetroPie/roms/psx/Metal Gear Solid.m3u
```
