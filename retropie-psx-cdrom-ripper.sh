#!/usr/bin/env bash

################################################################################
# Ripping a PSX/PlayStation 1 CD-ROM to .bin and .cue files.
#
# These files are compliant with RetroPie stack.
#
# Invoke this script with --help options to learn more.
################################################################################

# >>>>>>>>>>>>>>>>>>>>>>>> variables >>>>>>>>>>>>>>>>>>>>>>>>
readonly DEFAULT_CDROM_DEVICE="/dev/sr0"
readonly DEFAULT_OUTPUT_DIR="${HOME}/RetroPie/roms/psx"
readonly DEFAULT_ROOT_TMP_DIR="/tmp"
readonly DEFAULT_DISC_NUMBER=1
readonly USAGE="${0} --name \"Video game name\" [--disc <number-of-discs>] [--device /dev/input-cdrom-device] [--outputdir /path/where/files/should/be/generated]"
# <<<<<<<<<<<<<<<<<<<<<<<< variables <<<<<<<<<<<<<<<<<<<<<<<<

# >>>>>>>>>>>>>>>>>>>>>>>> functions >>>>>>>>>>>>>>>>>>>>>>>>

function help() {
  cat <<EOF
DESCRIPTION

    Rip a PSX/PlayStation 1 game to generate files are compliant with RetroPie stack.

    By default, generated files are automatically deployed into the RetroPie directory corresponding to PSX games (${DEFAULT_OUTPUT_DIR}).

    Then, you have just to restart EmulationStation and the game should automatically appear in the PlayStation section.

    If the game has multiple CD-ROMs, you must set the number of CD-ROM using option --disc <number-of-discs>.

DEPENDENCIES

    This script uses "cdrdao" and "toc2cue" utilities. You must install them before launching this script.

    On Debian, you can install them using APT:
        apt install cdrdao toc2cue

OPTIONS

    --device <device-path>
        Path to CD-ROM device.
        Default: ${DEFAULT_CDROM_DEVICE}

    -d <number-of-discs>, --disc <number-of-discs>
        Number of discs to rip for this game.
        Default: ${DEFAULT_DISC_NUMBER}

    -n <name>, --name <name>
        Name of the game.
        It is used to determine the filenames.
        Mandatory.

    -o <output-dir-path>, --outputdir <output-dir-path>
        Path to output directory.
        This directory must exist.
        Default: ${DEFAULT_OUTPUT_DIR}

    --help, -h
        Print this message.

EXAMPLE

    Process Tony Hawk's Pro Skater 2 game and deploy files into RetroPie PSX folder.
    This game is a single-disc game with only 1 CD-ROM.
        ${0} --name "Tony Hawk's Pro Skater 2"

    Process Metal Gear Solid game and deploy files into RetroPie PSX folder.
    This game is a multi-disc game with 2 CD-ROMs.
        ${0} --name "Metal Gear Solid" --disc 2
EOF
}

function log() {
  local msg
  msg="$(date +"%H:%M:%S") [${1}] ${2}"

  if [[ "${1}" == "ERROR" ]]; then
    echo -e "${msg}" 2>&1
  else
    echo -e "${msg}"
  fi
}

function error() {
  log "ERROR" "${1}"
}

function info() {
  log "INFO" "${1}"
}

function exit_on_error() {
  error "${1}"

  delete_tmp_dir

  if [[ "x" != "x${2}" ]]; then
    exit "${2}"
  else
    exit 2
  fi
}

function check_command() {
  if ! command -v "${1}" &>/dev/null; then
    exit_on_error "Command '${1}' is not available."
  fi
}

function create_tmp_dir() {
  info "Creating temporary directory ${TMP_DIR}..."
  if ! mkdir -p "${TMP_DIR}"; then
    exit_on_error "Creating temporary directory ${TMP_DIR} has failed."
  fi
}

function delete_tmp_dir() {
  if [[ -d "${TMP_DIR}" ]]; then
    info "Removing temporary directory ${TMP_DIR}..."
    if ! rm -rf "${TMP_DIR}"; then
      error "Removing temporary directory ${TMP_DIR} has failed."
    fi
  fi
}

function rip_cdrom() {
  local bin_filename cue_filename toc_filename
  local tmp_bin_path tmp_cue_path tmp_toc_path
  local output_bin_path output_cue_path
  bin_filename="${1}"
  cue_filename="${2}"
  toc_filename="${3}"
  tmp_bin_path="${TMP_DIR}/${bin_filename}"
  tmp_cue_path="${TMP_DIR}/${cue_filename}"
  tmp_toc_path="${TMP_DIR}/${toc_filename}"
  output_bin_path="${OUTPUT_DIR}/${bin_filename}"
  output_cue_path="${OUTPUT_DIR}/${cue_filename}"

  info "Ripping CD-ROM..."
  info " - Temporary .bin path:   ${tmp_bin_path}"
  info " - Temporary .cue path:   ${tmp_cue_path}"
  info " - Temporary .toc path:   ${tmp_toc_path}"
  info " - Output .bin path:      ${output_bin_path}"
  info " - Output .cue path:      ${output_cue_path}"
  if ! cdrdao read-cd --read-raw --datafile "${tmp_bin_path}" --device "${CDROM_DEVICE}" --driver generic-mmc-raw "${tmp_toc_path}"; then
    exit_on_error "Ripping CD-ROM has failed."
  fi

  info "Generating .cue file..."
  if ! toc2cue "${tmp_toc_path}" "${tmp_cue_path}"; then
    exit_on_error "Generating .cue file has failed."
  fi

  info "Patching path in .cue file..."
  if ! sed -i "s@${tmp_bin_path}@${bin_filename}@" "${tmp_cue_path}"; then
    exit_on_error "Patching path in .cue file has failed."
  fi

  info "Deploying .bin and .cue file to output directory..."
  if ! mv "${tmp_bin_path}" "${output_bin_path}"; then
    exit_on_error "Moving ${tmp_bin_path} to ${output_bin_path} has failed."
  fi
  if ! mv "${tmp_cue_path}" "${output_cue_path}"; then
    exit_on_error "Moving ${tmp_cue_path} to ${output_cue_path} has failed."
  fi

  info "Removing temporary .toc file..."
  if ! rm -f "${tmp_toc_path}"; then
    exit_on_error "Removing temporary ${tmp_toc_path} file has failed."
  fi

  info "Operation completed for current CD-ROM:"
  info " - Binary file: ${output_bin_path}"
  info " - CUE file:    ${output_cue_path}"

  OUTPUT_FILES+=("${output_bin_path}")
  OUTPUT_FILES+=("${output_cue_path}")
}

function generate_m3u_file() {
  local m3u_filename tmp_m3u_path output_m3u_path
  m3u_filename="${NAME}.m3u"
  tmp_m3u_path="${TMP_DIR}/${m3u_filename}"
  output_m3u_path="${OUTPUT_DIR}/${m3u_filename}"

  info "Generating .m3u file..."
  info " - Temporary .m3u path:   ${tmp_m3u_path}"
  info " - Output .m3u path:      ${output_m3u_path}"
  if ! : >"${tmp_m3u_path}"; then
    exit_on_error "Generating .m3u file has failed."
  fi

  for index in $(seq 1 "${DISC_NUMBER}"); do
    if ! echo "${NAME} (Disc ${index}).CD${index}" >>"${tmp_m3u_path}"; then
      exit_on_error "Generating .m3u file has failed."
    fi
  done

  info "Deploying .m3u file to output directory..."
  if ! mv "${tmp_m3u_path}" "${output_m3u_path}"; then
    exit_on_error "Moving ${tmp_m3u_path} to ${output_m3u_path} has failed."
  fi

  OUTPUT_FILES+=("${output_m3u_path}")
}

# <<<<<<<<<<<<<<<<<<<<<<<< functions <<<<<<<<<<<<<<<<<<<<<<<<

# >>>>>>>>>>>>>>>>>>>>>>>> argument parsing >>>>>>>>>>>>>>>>>>>>>>>>

CDROM_DEVICE="${DEFAULT_CDROM_DEVICE}"
unset NAME
OUTPUT_DIR="${DEFAULT_OUTPUT_DIR}"
TMP_DIR="${DEFAULT_ROOT_TMP_DIR}/retropie-psx-cdrom-ripper"
DISC_NUMBER="${DEFAULT_DISC_NUMBER}"

while (($# > 0)); do
  case "${1}" in
  --device)
    DEFAULT_CDROM_DEVICE="${2}"
    shift 2
    ;;

  -d | --disc)
    if [[ ! "${2}" =~ ^[1-9][0-9]*$ ]]; then
      exit_on_error "Value ${2} is not valid for option '${1}'.\n[USAGE] ${USAGE}"
    fi
    DISC_NUMBER="${2}"
    shift 2
    ;;

  -n | --name)
    NAME="${2}"
    shift 2
    ;;

  -o | --outputdir)
    OUTPUT_DIR="${2}"
    shift 2
    ;;

  -h | --help)
    help
    exit 0
    ;;

  *)
    exit_on_error "Invalid option '${1}'.\n[USAGE] ${USAGE}"
    ;;
  esac
done

check_command "cdrdao"
check_command "toc2cue"

if [[ -z "${NAME}" ]]; then
  exit_on_error "Option --name is missing. You must define the name of the video game.\n[USAGE] ${USAGE}"
fi

if [[ ! -d "${OUTPUT_DIR}" ]]; then
  exit_on_error "Output directory '${OUTPUT_DIR}' does not exist.\n[USAGE] ${USAGE}"
fi

if [[ ! -b "${CDROM_DEVICE}" ]]; then
  exit_on_error "CD-ROM device '${CDROM_DEVICE}' does not exist or is not a block device.\n[USAGE] ${USAGE}"
fi

# <<<<<<<<<<<<<<<<<<<<<<<< argument parsing <<<<<<<<<<<<<<<<<<<<<<<<

# >>>>>>>>>>>>>>>>>>>>>>>> main >>>>>>>>>>>>>>>>>>>>>>>>

function main() {
  info "Video game:          ${NAME}"
  info "CD-ROM device:       ${CDROM_DEVICE}"
  if [ "${DISC_NUMBER}" -eq 1 ]; then
    info "Type :               Single-disc game (only 1 CD-ROM)"
  else
    info "Type :               Multi-discs games (${DISC_NUMBER} CD-ROMS)"
  fi
  info "Output directory:    ${OUTPUT_DIR}"
  info "Temporary directory: ${TMP_DIR}"

  delete_tmp_dir
  create_tmp_dir

  OUTPUT_FILES=()

  for index in $(seq 1 "${DISC_NUMBER}"); do
    info "===== Ripping CD-ROM n°${index}/${DISC_NUMBER} ====="

    # Determine filenames
    if [[ "${DISC_NUMBER}" -eq 1 ]]; then
      bin_filename="${NAME}.bin"
      cue_filename="${NAME}.cue"
      toc_filename="${NAME}.toc"
    else
      bin_filename="${NAME} (Disc ${index}).bin"
      cue_filename="${NAME} (Disc ${index}).CD${index}"
      toc_filename="${NAME} (Disc ${index}).toc"
    fi

    rip_cdrom "${bin_filename}" "${cue_filename}" "${toc_filename}"

    if [[ "${index}" -lt "${DISC_NUMBER}" ]]; then
      echo
      read -rp "Please, insert next CD. Then, press ENTER to start ripping it."
    else
      info "All CD-ROMs have been ripped (${DISC_NUMBER} CD-ROMs)."
    fi
  done

  if [[ ${DISC_NUMBER} -gt 1 ]]; then
    info "===== Generating .m3u file for multi-disc game ====="
    generate_m3u_file
  fi

  delete_tmp_dir

  info "===== Operation completed ====="
  info "Output files:"
  for path in "${OUTPUT_FILES[@]}"; do
    info " - ${path}"
  done
}

main

# <<<<<<<<<<<<<<<<<<<<<<<< main <<<<<<<<<<<<<<<<<<<<<<<<
